<?php include ('layouts/head.php'); ?>

<?php include ('layouts/white-navbar.php'); ?>

<main class="white-page starter-page">

   <div class="container title-page">
      <div class="row">
         <div class="col col-lg-4">                
            <img src="img/empreendimentos/privilege/fachada.jpg">
            </div>
            <div class="col col-lg-7">
               <p class="line bg-blue"></p>     
               <h1 class="mt-20">Privilége</h1>
               <p><b>ENCONTRE O SEU LUGAR. VIVA CERCADO PELA QUALIDADE DE VIDA.</b></p>
               <p>Visite nosso estande e conheça o apartamento decorado.</p>
         </div>
      </div>      
   </div>   

   <div class="container">
      <div class="row gallery-row">      
         <div class="gallery-column">
            <img src="img/empreendimentos/privilege/01.jpg">
            <img src="img/empreendimentos/privilege/02.jpg">
            <img src="img/empreendimentos/privilege/03.jpg">
            <img src="img/empreendimentos/privilege/04.jpg">
            <img src="img/empreendimentos/privilege/18.jpg">
            <img src="img/empreendimentos/privilege/19.jpg">
            <img src="img/empreendimentos/privilege/20.jpg">
         </div>
         <div class="gallery-column">       
            <img src="img/empreendimentos/privilege/05.jpg">  
            <img src="img/empreendimentos/privilege/06.jpg">
            <img src="img/empreendimentos/privilege/07.jpg">
            <img src="img/empreendimentos/privilege/08.jpg">        
            <img src="img/empreendimentos/privilege/21.jpg">
         </div>     
         <div class="gallery-column">
            <img src="img/empreendimentos/privilege/10.jpg">
            <img src="img/empreendimentos/privilege/11.jpg"> 
            <img src="img/empreendimentos/privilege/12.jpg">
            <img src="img/empreendimentos/privilege/13.jpg">  
            <img src="img/empreendimentos/privilege/22.jpg">
         </div>     
         <div class="gallery-column">
            <img src="img/empreendimentos/privilege/14.jpg">
            <img src="img/empreendimentos/privilege/15.jpg">
            <img src="img/empreendimentos/privilege/16.jpg">
            <img src="img/empreendimentos/privilege/17.jpg">            
            <img src="img/empreendimentos/privilege/09.jpg">
            <img src="img/empreendimentos/privilege/23.jpg">
         </div>   
      </div>   
   </div>
      
</main>

<?php include ('layouts/white-footer.php'); ?>