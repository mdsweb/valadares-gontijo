<?php include ('layouts/head.php'); ?>

<main class="blue-page">

	<?php include ('layouts/home-navbar.php'); ?>

	<video poster="img/slide1.jpg" id="bgvid" style="width:100%;margin-top:-50px" playsinline autoplay loop controls>
		<source src="video-min.mp4" type="video/mp4">
	</video>
	
	<!-- Slide da home
	<div id="carousel-header" class="carousel slide" data-ride="carousel">
		<ol class="carousel-indicators">
			<li data-target="#carousel-header" data-slide-to="0" class="active">01</li>
			<li data-target="#carousel-header" data-slide-to="1">02</li>
		</ol>
		<div class="carousel-inner">
			<div class="carousel-item active">
				<img src="img/slide1.jpg" class="d-block w-100" alt="...">
				<div class="carousel-caption d-none d-md-block">
					<span>Grand Villaggio</span>
					<h1>Além do que seus sonhos imaginam</h1>
					<p>Empreendimento completo e diferenciado.</p>
					<a href="#!">Veja o projeto</a>
				</div>
			</div>
			<div class="carousel-item">
				<img src="img/slide1.jpg" class="d-block w-100" alt="...">
				<div class="carousel-caption d-none d-md-block">
					<span>Grand Villaggio</span>
					<h1>+ de 25 Anos</h1>
					<p>Abrindo portas para as novas histórias</p>
					<a href="#!">Veja o projeto</a>
				</div>
			</div>
		</div>
		<a class="carousel-control-prev" href="#carousel-header" role="button" data-slide="prev">
			<span class="carousel-control-prev-icon" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		</a>
		<a class="carousel-control-next" href="#carousel-header" role="button" data-slide="next">
			<span class="carousel-control-next-icon" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</a>
	</div>  -->

	<!-- Redes sociais
	<div class="header__social">
		<ul>
			<li>
				<a href="#!"><i class="fab fa-twitter"></i></a>
			</li>
			<li>
				<a href="#!"><i class="fab fa-facebook"></i></a>
			</li>
			<li>
				<a href="#!"><i class="fab fa-instagram"></i></a>
			</li>         
		</ul>
	</div>

	<div class="language">
		<ul>
			<li>
				<a href="#!">Pt</a>
			</li>
			<li>
				<a href="#!">Eua</a>
			</li>
			<li>
				<a href="#!">Spa</a>
			</li>
		</ul>
	</div> -->

</header>

<div id="projects">
	<div class="container">

		<div class="row section-title">
			<div class="col-2">
				<b>Nossos projetos</b>
			</div>
			<div class="col-8">
				<!-- <ul>
					<li><a href="#!">Todos</a></li>
					<li><a href="#!">Belo Horizonte</a></li>
					<li><a href="#!">Campinas</a></li>
				</ul> -->
				<!-- <select name="sort-news" id="sort-news">
					<option value="All">All news</option>
					<option value="Cat1">Category 1</option>
					<option value="Cat2">Category 2</option>
					<option value="Cat3">Category 3</option>
				</select>  -->
			</div>
			<div class="col-2 text-right">
				<!-- setas -->
			</div>
		</div>

		<section class="news-list">
			<div class="row">
				<article class="col-4 news-item" data-category="Cat1">
					<a href="convivence.php">
						<figure>
							<img src="img/projetos/convivence.jpg" alt="Convivence" title="Convivence">
						</figure>
					</a>
				</article>
				<article class="col-4 news-item" data-category="Cat3">
					<a href="don-cambui.php">
						<figure>
							<img src="img/projetos/don.jpg" alt="Don Cambuí" title="Don Cambuí">
						</figure>
					</a>
				</article>
				<article class="col-4 news-item" data-category="Cat1">
					<a href="grand-villagio.php">
						<figure>
							<img src="img/projetos/grand-villagio.jpg" alt="Grand Villagio" title="Grand Villagio">
						</figure>
					</a>
				</article>
				<article class="col-4 news-item" data-category="Cat1">
					<a href="varandas-da-pampulha.php">
						<figure>
							<img src="img/projetos/varandas.jpg" alt="Varandas da Pampulha" title="Varandas da Pampulha">
						</figure>
					</a>
				</article>
				<article class="col-4 news-item" data-category="Cat3">
					<a href="privilege.php">
						<figure>
							<img src="img/projetos/privilege.jpg" alt="Privilege" title="Privilege">
						</figure>
					</a>
				</article>
			</div>
		</section>

	</div>
</div>

<div id="portfolio">
	<div class="container">

		<div class="row section-title">
			<div class="col">
				<b>Portfólio</b>
			</div>
			<div class="col text-right">
				<a href="portfolio.php">Veja todos empreendimentos entregues > </a>
			</div>
		</div>

		<div class="row">
			<article class="col-6">
				<figure>
					<a href="privilege.php">
						<img src="img/portfolio/privilege.jpg"  alt="Privilége" title="Privilége" />
					</a>
				</figure>
				<h1>Privilége</h1>
			</article>
			<article class="col-6">
				<figure>
					<a href="varandas-da-pampulha.php">
						<img src="img/portfolio/varandas.jpg"  alt="Varandas da Pampulha" title="Varandas da Pampulha" />
					</a>
				</figure>
				<h1>Varandas da Pampulha</h1>
			</article>
		</div>

	</div>
</div>



</main>

<?php include ('layouts/blue-footer.php'); ?>