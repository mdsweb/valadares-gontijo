$( document ).ready(function() {

	//OPEN NAV ON HAMBURGUER CLICK
	$(".hamburguer").click(function(){
		$("#main-menu").toggleClass("d-block");  
		$(this).toggleClass("toggle");
		$("nav ul li").click(function(){
			$("#main-menu").removeClass("d-block")
			$(".hamburguer").removeClass("toggle");
		});
	});

	//Video home
	$("#bgvid").prop('muted', true);

	$("#bgvid").click( function (){
		if( $("#bgvid").prop('muted') ) {
				$("#bgvid").prop('muted', false);
		} else {
			$("#bgvid").prop('muted', true);
		}
	});


	pauseButton.addEventListener("click", function() {
	vid.classList.toggle("stopfade");
	if (vid.paused) {
		vid.play();
		pauseButton.innerHTML = "Pause";
	} else {
		vid.pause();
		pauseButton.innerHTML = "Paused";
	}
	})


	


	//Portfolio select filter
	$('select#sort-news').change(function() {
		var filter = $(this).val()
		filterList(filter);
	});

	//Portfolio item filter
	function filterList(value) {
		var list = $(".news-list .news-item");
		$(list).fadeOut("fast");
		if (value == "All") {
			$(".news-list").find("article").each(function (i) {
				$(this).delay(200).fadeIn("fast");
			});
		} else {
			$(".news-list").find("article[data-category*=" + value + "]").each(function (i) {
				$(this).delay(200).fadeIn("fast");
			});
		}
	}

	//MAPA DA PAGINA DE CONTATO
	$('.contact-map').addClass('scrolloff');                // set the mouse events to none when doc is ready        
	$('#overlay').on("mouseup",function(){          // lock it when mouse up
		$('.contact-map').addClass('scrolloff'); 
		//somehow the mouseup event doesn't get call...
	});
	$('#overlay').on("mousedown",function(){        // when mouse down, set the mouse events free
		$('.contact-map').removeClass('scrolloff');
	});
	$(".contact-map").mouseleave(function () {              // becuase the mouse up doesn't work... 
		$('.contact-map').addClass('scrolloff');            // set the pointer events to none when mouse leaves the map area
																	// or you can do it on some other event
	});

	

})