<?php include ('layouts/head.php'); ?>

<?php include ('layouts/white-navbar.php'); ?>

<main class="white-page starter-page">

   <div class="container title-page">
      <div class="row">
         <div class="col col-lg-4">                
            <img src="img/empreendimentos/don/fachada.jpg">
         </div>
         <div class="col col-lg-7">
            <p class="line bg-blue"></p>     
            <h1 class="mt-20">DON Cambuí</h1>
            <p><b>ENCONTRE O SEU LUGAR. VIVA CERCADO PELA QUALIDADE DE VIDA.</b></p>
            <p>Visite nosso estande e conheça o apartamento decorado.</p>
         </div>         
      </div>      
   </div>  

   <div class="container">   
      <div class="row gallery-row mt-40">      
         <div class="gallery-column">
            <img src="img/empreendimentos/don/01.jpg">
            <img src="img/empreendimentos/don/02.jpg">  
            <img src="img/empreendimentos/don/03.jpg">    
            <img src="img/empreendimentos/don/04.jpg">                  
         </div>
         <div class="gallery-column">                      
            <img src="img/empreendimentos/don/05.jpg">  
            <img src="img/empreendimentos/don/06.jpg"> 
            <img src="img/empreendimentos/don/07.jpg">    
            <img src="img/empreendimentos/don/08.jpg">                  
         </div>     
         <div class="gallery-column">
            <img src="img/empreendimentos/don/09.jpg">  
            <img src="img/empreendimentos/don/10.jpg"> 
            <img src="img/empreendimentos/don/11.jpg">    
            <img src="img/empreendimentos/don/12.jpg">     
         </div>     
         <div class="gallery-column">
            <img src="img/empreendimentos/don/13.jpg"> 
            <img src="img/empreendimentos/don/14.jpg">    
            <img src="img/empreendimentos/don/15.jpg">
         </div>   
      </div>  
   </div>    
      
</main>

<?php include ('layouts/white-footer.php'); ?>