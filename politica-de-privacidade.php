<?php include ('layouts/head.php'); ?>

<?php include ('layouts/white-navbar.php'); ?>

<main class="white-page starter-page">

   <div class="container title-page">
      <div class="row">
         <div class="col col-3"><p class="line bg-blue"></p></div>
         <div class="col col-8">
            <h1>Política<br>de Privacidade</h1>
         </div>
      </div>      
   </div>
      
   <div class="container">
      <div class="row">
         <div class="col col-3"></div>
         <div class="text-page text-justify">
            <p>Os dados fornecidos por você no site da Construtora Valadares Gontijo contribuem para uma melhor prestação de nossos serviços e ainda nos ajudam a melhorar o nosso site, tornando-o cada vez mais direcionado às necessidades e informações que os usuários buscam.</p>
            <p>Seus dados são solicitados através de formulários quando você deseja receber/acessar mais informações sobre nossos produtos ou ser atendido(a) pela empresa.</p>
            <p>Consideramos as informações dos usuários de nosso site importantes e sigilosas. Colhemos informações pessoais como nome, telefone e outras quando fornecidas voluntariamente através dos formulários de cadastro. Estas informações em hipótese alguma serão compartilhadas, publicadas ou comercializadas com terceiros. Salvo se a empresa for obrigada por lei, processo ou determinação de autoridades a fazê-lo para preservar a segurança de outros usuários ou identificar algum crime.</p>
            <p>A Construtora Valadares Gontijo não se responsabiliza por qualquer dano direto ou indireto ocasionado por eventos de terceiros, como ataque de hackers, falhas no sistema, no servidor ou na conexão à internet, inclusive por ações de softwares maliciosos como vírus, cavalos de Tróia, e outros que possam, de algum modo, danificar o equipamento ou a conexão dos usuários em decorrência do acesso, utilização ou navegação no nosso site, bem como a transferência de dados, arquivos, imagens, textos, áudios ou vídeos contidos neste.</p>
            <p>A Construtora Valadares Gontijo não garante que o conteúdo - sejam informações, valores ou imagens - oferecido neste website esteja precisamente atualizado ou completo, e não se responsabiliza por danos causados por eventuais erros de conteúdo ou falhas de equipamento do usuário.</p>
            <span>Dicas importantes</span>
            <p>A Construtora Valadares Gontijo nunca envia e-mails pedindo seus dados pessoais (informações de pagamento/cadastro), solicitando download ou execução de arquivos, como por exemplo: extensão exe, com, scr, bat, entre outros. Em caso de dúvidas, por favor entre em contato com nosso SAC.</p>
            <span>Cookies</span>
            <p>A Construtora Valadares Gontijo utiliza cookies e informações de sua navegação para traçar o seu perfil e, assim, oferecer-lhe serviços cada vez mais convenientes e personalizados. Estamos trabalhando em constante melhoria para aperfeiçoar nossos serviços, conteúdo e navegação. Vale lembrar que os dados armazenados são de uso exclusivo da Construtora Valadares Gontijo. Assim, você tem a garantia de que seus dados serão mantidos no mais absoluto sigilo. Se você já é cadastrado no site e não deseja receber nossas comunicações exclusivas para clientes Construtora Valadares Gontijo, enviadas para o seu e-mail, basta clicar no link de opt out no rodapé do e-mail marketing.<p>O usuário tem a possibilidade de configurar seu navegador para ser avisado, na tela do computador, sobre a recepção dos cookies e para impedir a sua instalação no disco rígido. As informações pertinentes à esta configuração estão disponíveis em instruções e manuais do próprio navegador. </p><p>A Construtora Valadares Gontijo poderá realizar retificações e mudanças nesta Política de Privacidade sempre que houver necessidade. Este documento não cria qualquer vínculo contratual entre o site e o usuário.</p><p>Em caso de dúvidas e sugestões em relação a nossa política de privacidade, por favor entre em contato conosco: Tel: <b>(31) 3291-3919</b> ou envie um e-mail para <b>sac@valadaresgontijo.com.br</b></p>
         </div>
      </div>    
   </div>      

</main>

<?php include ('layouts/white-footer.php'); ?>