<?php include ('layouts/head.php'); ?>

<?php include ('layouts/white-navbar.php'); ?>

<main class="white-page starter-page">

   <div class="container title-page">
      <div class="row">
         <div class="col col-3"><p class="line bg-blue"></p></div>
         <div class="col col-8">
            <h1>Mais de 25 anos abrindo portas<br>para as novas histórias.</h1>
         </div>
      </div>      
   </div>
      
   <div class="container-fluid">
      <div class="row">
         <div class="col">
            <img class="image-page" src="img/sobre.jpg" alt="">
         </div>            
      </div>
   </div>
      

   <div class="container">
      <div class="row">
         <div class="col col-3">
            <p class="line bg-blue"></p>
            <p class="subtitle-page">Sobre<br>nós</p>
         </div>
            <div class="col col-8 text-page text-justify">
            <span>Os valores são nossos alicerces.</span>
            <p>A Valadares Gontijo começou a construir histórias em 1992, em Minas Gerais. O compromisso da construtora é levar o alto padrão em tudo que faz. Pensa sempre em soluções inovadoras e criativas feitas para pessoas que transformarão o que a  gente chama de imóvel em um verdadeiro lar, cheio de vida, amor e lembranças boas de se recordar.</p>
            <p>Ética, honestidade e transparência são nossos pilares de sustentação. Todo imóvel que construímos deve ter um equilíbrio entre os valores estéticos e o conforto e bem-estar de seus moradores. Tudo para oferecer um atendimento acima das expectativas dos clientes e imóveis de acordo com os seus melhores sonhos.</p>
         </div>
      </div>   

      <div class="row mt-100">
         <div class="col col-3">
            <p class="line bg-blue"></p>
            <p class="subtitle-page">O que<br>fazemos</p>
         </div>
         <div class="col col-8">
            <div class="row text-page">               
               <div class="col col-4">
                  <span><b>Responsabilidade</b></span>
                  <p>A Valadares Gontijo constrói com responsabilidade ambiental, preservando áreas verdes, controlando o desperdício de materiais e fazendo o descarte correto de entulhos. Construir um lar de verdade é não se esquecer do principal: o nosso planeta.</p>
               </div>
               <div class="col col-4">
                  <span><b>Futuro</b></span>
                  <p>Construímos pensando no futuro. Investimos em materiais de primeira qualidade e não abrimos mão da sustentabilidade. Assim, sua família poderá desfrutar do que há de melhor sem precisar sair de casa.</p>
               </div>
               <div class="col col-4">
                  <span><b>Empreendimentos</b></span>
                  <p>A Valadares Gontijo está construindo o seu sonho. Conheça os empreendimentos disponíveis para vendas. Escolha um deles pra chamar de sua casa.</p>
               </div>
            </div>
         </div>
      </div>   
   </div>
      
</main>

<?php include ('layouts/white-footer.php'); ?>