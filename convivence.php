<?php include ('layouts/head.php'); ?>

<?php include ('layouts/white-navbar.php'); ?>

<main class="white-page starter-page">

   <div class="container title-page">
      <div class="row">
         <div class="col col-lg-4">                
            <img src="img/empreendimentos/convivence/fachada.jpg">
            </div>
            <div class="col col-lg-7">
               <p class="line bg-blue"></p>     
               <h1 class="mt-20">Convivence</h1>
               <p>Mais do que viver junto, viver em harmonia. Harmonia para você curtir a família, para receber os velhos amigos e para cultivar novas amizades. O espaço do Convivence foi planejado tendo as pessoas como o centro de tudo. Seja dentro do apartamento ou na área de lazer, o bem-estar vai circular livremente nos seus dias.</p>
               <p><b>ENCONTRE O SEU LUGAR. VIVA CERCADO PELA QUALIDADE DE VIDA.</b></p>
               <p>Visite nosso estande e conheça o apartamento decorado.</p>
         </div>
      </div>      
   </div>      

   <div class="container-fluid">
      <div class="row">
         <div class="col-lg-12">
            <img src="img/empreendimentos/convivence/pdf/01.jpg" alt="">        
            <img src="img/empreendimentos/convivence/pdf/02.png" alt="">
            <img src="img/empreendimentos/convivence/pdf/03.png" alt="">
            <img src="img/empreendimentos/convivence/pdf/04.png" alt="">
            <img src="img/empreendimentos/convivence/pdf/05.png" alt="">
            <img src="img/empreendimentos/convivence/pdf/06.png" alt="">
            <img src="img/empreendimentos/convivence/pdf/07.png" alt="">
            <img src="img/empreendimentos/convivence/pdf/08.png" alt="">
            <img src="img/empreendimentos/convivence/pdf/09.png" alt="">
            <img src="img/empreendimentos/convivence/pdf/10.png" alt="">
            <img src="img/empreendimentos/convivence/pdf/11.png" alt="">
            <img src="img/empreendimentos/convivence/pdf/12.png" alt="">
         </div>
      </div>
   </div>

   <div class="container mt-60">
      <div class="row gallery-row">      
         <div class="gallery-column">
            <img src="img/empreendimentos/convivence/01.jpg">
            <img src="img/empreendimentos/convivence/02.jpg">
            <img src="img/empreendimentos/convivence/03.jpg">           
         </div>
         <div class="gallery-column">  
            <img src="img/empreendimentos/convivence/04.jpg">
            <img src="img/empreendimentos/convivence/05.jpg">                           
         </div>     
         <div class="gallery-column">
            <img src="img/empreendimentos/convivence/06.jpg">  
            <img src="img/empreendimentos/convivence/07.jpg">
            <img src="img/empreendimentos/convivence/08.jpg">
         </div>     
         <div class="gallery-column">
            <img src="img/empreendimentos/convivence/09.jpg">
            <img src="img/empreendimentos/convivence/10.jpg">
         </div>   
      </div>   
   </div>
      
</main>

<?php include ('layouts/white-footer.php'); ?>