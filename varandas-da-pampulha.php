<?php include ('layouts/head.php'); ?>

<?php include ('layouts/white-navbar.php'); ?>

<main class="white-page starter-page">

   <div class="container title-page">
      <div class="row">
         <div class="col col-lg-4">                
            <img src="img/empreendimentos/varandas-da-pampulha/fachada.jpeg">
            </div>
            <div class="col col-lg-7">
               <p class="line bg-blue"></p>     
               <h1 class="mt-20">Varandas da Pampuhla</h1>
               <p><b>ENCONTRE O SEU LUGAR. VIVA CERCADO PELA QUALIDADE DE VIDA.</b></p>
               <p>Visite nosso estande e conheça o apartamento decorado.</p>
         </div>
      </div>      
   </div>   

   <div class="container">
      <div class="row gallery-row">      
         <div class="gallery-column">
            <img src="img/empreendimentos/varandas-da-pampulha/01.jpg">
            <img src="img/empreendimentos/varandas-da-pampulha/02.jpg">
            <img src="img/empreendimentos/varandas-da-pampulha/03.jpg">
            <img src="img/empreendimentos/varandas-da-pampulha/04.jpg">
            <img src="img/empreendimentos/varandas-da-pampulha/05.jpg">
            <img src="img/empreendimentos/varandas-da-pampulha/06.jpg">
            <img src="img/empreendimentos/varandas-da-pampulha/07.jpg">
            <img src="img/empreendimentos/varandas-da-pampulha/08.jpg">
            <img src="img/empreendimentos/varandas-da-pampulha/09.jpg">
            <img src="img/empreendimentos/varandas-da-pampulha/10.jpg">
            <img src="img/empreendimentos/varandas-da-pampulha/11.jpg">
            <img src="img/empreendimentos/varandas-da-pampulha/12.jpg">
            <img src="img/empreendimentos/varandas-da-pampulha/33.jpg">
            <img src="img/empreendimentos/varandas-da-pampulha/34.jpg">
         </div>
         <div class="gallery-column">       
            <img src="img/empreendimentos/varandas-da-pampulha/13.jpg">
            <img src="img/empreendimentos/varandas-da-pampulha/14.jpg">
            <img src="img/empreendimentos/varandas-da-pampulha/15.jpg">
            <img src="img/empreendimentos/varandas-da-pampulha/16.jpg">
            <img src="img/empreendimentos/varandas-da-pampulha/17.jpg">
            <img src="img/empreendimentos/varandas-da-pampulha/18.jpg">            
            <img src="img/empreendimentos/varandas-da-pampulha/19.jpg">
            <img src="img/empreendimentos/varandas-da-pampulha/20.jpg">
            <img src="img/empreendimentos/varandas-da-pampulha/21.jpg">
            <img src="img/empreendimentos/varandas-da-pampulha/22.jpg">
            <img src="img/empreendimentos/varandas-da-pampulha/23.jpg">
            <img src="img/empreendimentos/varandas-da-pampulha/24.jpg">
            <img src="img/empreendimentos/varandas-da-pampulha/35.jpg">            
         </div>     
         <div class="gallery-column">
            <img src="img/empreendimentos/varandas-da-pampulha/25.jpg">
            <img src="img/empreendimentos/varandas-da-pampulha/26.jpg">
            <img src="img/empreendimentos/varandas-da-pampulha/27.jpg">
            <img src="img/empreendimentos/varandas-da-pampulha/28.jpg">
            <img src="img/empreendimentos/varandas-da-pampulha/29.jpg">
            <img src="img/empreendimentos/varandas-da-pampulha/30.jpg">            
            <img src="img/empreendimentos/varandas-da-pampulha/31.jpg">
            <img src="img/empreendimentos/varandas-da-pampulha/32.jpg">
            <img src="img/empreendimentos/varandas-da-pampulha/36.jpg"> 
         </div>     
         <div class="gallery-column">
            <img src="img/empreendimentos/varandas-da-pampulha/37.jpg">
            <img src="img/empreendimentos/varandas-da-pampulha/38.jpg">
            <img src="img/empreendimentos/varandas-da-pampulha/39.jpg">
            <img src="img/empreendimentos/varandas-da-pampulha/40.jpg">
            <img src="img/empreendimentos/varandas-da-pampulha/41.jpg">
            <img src="img/empreendimentos/varandas-da-pampulha/42.jpg">            
            <img src="img/empreendimentos/varandas-da-pampulha/43.jpg">
            <img src="img/empreendimentos/varandas-da-pampulha/44.jpg">
            <img src="img/empreendimentos/varandas-da-pampulha/45.jpg">
            <img src="img/empreendimentos/varandas-da-pampulha/46.jpg">
            <img src="img/empreendimentos/varandas-da-pampulha/47.jpg">
         </div>   
      </div>   
   </div>
      
</main>

<?php include ('layouts/white-footer.php'); ?>