<?php include ('layouts/head.php'); ?>

<?php include ('layouts/blue-navbar.php'); ?>

<main class="blue-page contato">

	<div id="overlay">
		<iframe class="contact-map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3750.7302505439993!2d-43.95003348508528!3d-19.93576898660059!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xa697634d80e9cf%3A0x725ef388fac4aac6!2sR.%20Bernardo%20Mascarenhas%2C%2046%20-%20Cidade%20Jardim%2C%20Belo%20Horizonte%20-%20MG%2C%2030380-010!5e0!3m2!1spt-BR!2sbr!4v1568076351435!5m2!1spt-BR!2sbr" width="100%" height="400" frameborder="0" allowfullscreen=""></iframe>
	</div>
	
	<div class="container">
		<div class="row section-title">
			<div class="col col-5">
				<b class="orange">CONTATO</b>
			</div>
			<div class="col col-7">
				<b>FALE CONOSCO</b>		
			</div>
		</div>

		<div class="row">
			<div class="col col-5">
				<h1>Belo Horizonte,<h2 class="orange">Brasil</h2></h1>
				<p>R. Fernandes Tourinho, 669<br>6° andar - Funcionários<br>Belo Horizonte/MG<br>CEP 30.112-002</p>
			</div>
			<div class="col col-7">
				<p>Fale com a Valadares Gontijo.<br>
				É sempre um prazer poder falar com você.<br>
				Entre em contato através de um dos nossos canais de atendimento.</p>
				<br><br>
				<p>+55 (31) 3291 3919</p>
				<p><a href="mailto:sac@valadaresgontijo.com.br">sac@valadaresgontijo.com.br</a></p>		
			</div>
		</div>
	</div>


</main> 

<?php include ('layouts/blue-footer.php'); ?>