<footer class="white-footer">
	<div class="container-fluid">
		<div class="row">
			<div class="col col-2 logo">
				<img src="img/logo.png" alt="Valadares Gontijo Logo" title="Valadares Gontijo Logo">
			</div>		
			<div class="col col-3">
				<p>® Valadares e Gontijo - 2019<br>Todos os direitos reservados</p>				
			</div>
			<div class="col col-3">
				<b>Mapa do site</b>
				<ul>
					<li><a href="termos-e-condicoes.php">Termos e condições</a></li>
					<li><a href="politica-de-privacidade.php">Política de privacidade</a></li>
					<!-- <li><a href="#!">Ajuda</a></li>	 -->
				</ul>
			</div>
			<div class="col col-2">
				<ul>
					<li><a href="index.php">Home</a></li>
					<li><a href="sobre-nos.php">Sobre nós</a></li>
					<!-- <li><a href="#!">Empreendimentos</a></li> -->
					<li><a href="portfolio.php">Portfólio</a></li>
					<li><a href="https://portal.capys.com.br/Default.aspx?id={5621A60B-475D-4063-BFDB-49B55EADC8AA}" target="_blank">Área do cliente</a></li>
					<!-- <li><a href="#!">Vendas</a></li> -->
					<li><a href="contato.php">Contato</a></li>
				</ul>
			</div>
			<div class="col col-2 text-right social">		
				<a href="https://www.facebook.com/ValadaresGontijo/" target="_blank"><i class="fab fa-2x fa-facebook"></i></a>
				<a href="https://www.instagram.com/valadares_gontijo/" target="_blank"><i class="fab fa-2x fa-instagram"></i></a>			
			</div>			
		</div>
	</div>
</footer>

<script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>
	

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="js/main.js"></script>

</body>
</html>