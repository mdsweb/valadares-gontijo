<div class="white-navbar">
   <div class="container-fluid">
      <div class="row">
         <div class="col col-6 logo">
            <a href="index.php"><img src="img/logo.png" alt="" title="" /></a>
         </div>
         <div class="col col-3 fale-conosco">
            <p>FALE CONOSCO: <a href="tel:+553132913919">+55 (31) 3291 3919</a></p>
         </div>
         <div class="col col-2 social">       		
            <a href="https://www.facebook.com/ValadaresGontijo/" target="_blank"><i class="fab fa-facebook"></i></a>				
            <a href="https://www.instagram.com/valadares_gontijo/" target="_blank"><i class="fab fa-instagram"></i></a>				        
			</ul>
         </div>
         <div class="col col-1 hamburguer">
            <b>MENU</b>
            <div class="bar1"></div>
            <div class="bar2"></div>
         </div>
      </div>
   </div>
</div>