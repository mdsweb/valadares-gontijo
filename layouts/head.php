<!DOCTYPE html>
<html lang="pt">

<head>
	<meta charset="UTF-8">
	<title>Valadares Gontijo</title>
	<link rel="stylesheet" href="css/style.min.css">
	<link rel="stylesheet" href="css/bootstrap/bootstrap.min.css">
	<link rel="stylesheet" href="css/plugins/fontawesome.min.css">
	<link rel="stylesheet" href="css/gallery.css">
</head>

<body>

<!-- Menu lateral principal -->
<div id="main-menu">
	<div class="header-menu">
		<!-- <div class="language">
			 <ul>
				<li><a href="#!">Pt</a></li>
				<li><a href="#!">Eua</a></li>
				<li><a href="#!">Spa</a></li>
			</ul> 
		</div>
		<div class="find-mobile">
			Encontre seu imóvel
		</div>-->		
	</div>
	<nav>
		<ul>
			<li><a href="index.php">Home</a></li>
			<li><a href="sobre-nos.php">Sobre Nós</a></li>
			<!-- <li><a href="#!">Empreendimentos</a></li> -->
			<li><a href="portfolio.php">Portfólio</a></li>
			<li><a href="https://portal.capys.com.br/Default.aspx?id={5621A60B-475D-4063-BFDB-49B55EADC8AA}" target="_blank">Área do Cliente</a></li>
			<!-- <li><a href="vendas.php">Vendas</a></li> -->
			<li><a href="contato.php">Contato</a></li>
		</ul>			
	</nav>
	<div class="footer-menu">
		<ul class="social">
			<li>
				<a href="https://www.facebook.com/ValadaresGontijo/" target="_blank"><i class="fab fa-2x fa-facebook"></i></a>
			</li>
			<li>
				<a href="https://www.instagram.com/valadares_gontijo/" target="_blank"><i class="fab fa-2x fa-instagram"></i></a>
			</li>
		</ul>
	</div>
</div>