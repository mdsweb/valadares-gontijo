<?php include ('layouts/head.php'); ?>

<?php include ('layouts/white-navbar.php'); ?>

<main class="white-page starter-page">

   <div class="container title-page">
      <div class="row">
         <div class="col col-3"><p class="line bg-blue"></p></div>
         <div class="col col-8">
            <h1>Termos<br>e Condições</h1>
         </div>
      </div>      
   </div>
      
   <div class="container">
      <div class="row">
         <div class="col col-3"></div>
            <div class="text-page text-justify">
               <p>O seu e-mail será utilizado para enviarmos informações sobre seus pedidos e promoções exclusivas da Construtora Valadares Gontijo. Você receberá nossas comunicações mas, caso não deseje mais, basta clicar no link de opt out no rodapé do e-mail marketing.</p>
         </div>
      </div>    
   </div>      

</main>

<?php include ('layouts/white-footer.php'); ?>