<?php include ('layouts/head.php'); ?>

<main id="portfolio-page" class="blue-page">

	<?php include ('layouts/home-navbar.php'); ?>

	<div class="container mb-40">
		<div class="row section-title">
			<div class="col-2"></div>
			<div class="col-6 text-left">
				<b>Portfólio</b>
				<p class="line bg-orange mt-20 mb-20"></p>
				<h1>Quando construímos e entregamos as chaves de um empreendimento, ele passa a ser parte da nossa história.</h1>
			</div>
		</div>
	</div>

	<div class="container-fluid">

		<div class="row">
			<article class="col-6">
				<figure>
					<a href="privilege.php">
						<img src="img/portfolio/privilege.jpg" alt="Privilége" title="Privilége" />
					</a>
				</figure>
				<h1>Privilége</h1>
			</article>
			<article class="col-6">
				<figure>
					<a href="varandas-da-pampulha.php">
						<img src="img/portfolio/varandas.jpg" alt="Varandas da Pampulha" title="Varandas da Pampulha" />
					</a>
				</figure>
				<h1>Varandas da Pampulha</h1>
			</article>
		</div>

		<div class="row mt-40">
			<article class="col-6">
				<figure>
					<a href="giardino.php">
						<img src="img/portfolio/giardino.jpg"  alt="Giardino" title="Giardino" />
					</a>
				</figure>
				<h1>Giardino</h1>
			</article>
			<article class="col-6">
				<figure>
					<a href="convivence.php">
						<img src="img/portfolio/convivence.jpg"  alt="Convivence" title="Convivence" />
					</a>
				</figure>
				<h1>Convivence</h1>
			</article>
		</div>

	</div>
</main>

<?php include ('layouts/blue-footer.php'); ?>