<?php include ('layouts/head.php'); ?>

<?php include ('layouts/white-navbar.php'); ?>

<main id="vendas" class="white-page starter-page">

   <div class="container title-page">
      <div class="row">
         <div class="col col-3"><p class="line bg-blue"></p></div>
         <div class="col col-8">
            <h1>Venha conhecer de perto<br>seu grande sonho.</h1>
         </div>
      </div>      
   </div>
      
   <div class="container">
      <div class="row">
         <article class="col-4">
            <figure>
               <img src="img/projetos/01.png" alt="Item" title="Item">
            </figure>
         </article>
         <article class="col-8 padding-40 title-page">
            <img src="img/logo.png" class="mb-40" alt="Item" title="Item">
            <h1>Valadares Gontijo.</h1>
            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Esse, libero veniam fuga aut eaque, illum natus sed quibusdam accusamus perspiciatis, commodi modi eveniet eius laboriosam corpori mmodi modi eveniet eius laboriosam corporis est dignissimos incidunt! Vel.</p>
         </article>
      </div>
      <div class="row">
         <article class="col-4">
            <figure>
               <img src="img/projetos/02.png" alt="Item" title="Item">
            </figure>
         </article>
         <article class="col-8 padding-40 title-page">
            <img src="img/logo.png" class="mb-40" alt="Item" title="Item">
            <h1>Valadares Gontijo.</h1>
            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Esse, libero veniam fuga aut eaque, illum natus sed quibusdam accusamus perspiciatis, commodi modi eveniet eius laboriosam corpori mmodi modi eveniet eius laboriosam corporis est dignissimos incidunt! Vel.</p>
         </article>
      </div>
   </div>
      
</main>

<?php include ('layouts/white-footer.php'); ?>