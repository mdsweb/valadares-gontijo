<?php include ('layouts/head.php'); ?>

<?php include ('layouts/white-navbar.php'); ?>

<main class="white-page starter-page">

   <div class="container title-page">
      <div class="row">
         <div class="col col-lg-4">                
            <img src="img/empreendimentos/grand-villagio/fachada.png">
            </div>
            <div class="col col-lg-7">
               <p class="line bg-blue"></p>     
               <h1 class="mt-20">Grand Villagio</h1>
               <p><b>ENCONTRE O SEU LUGAR. VIVA CERCADO PELA QUALIDADE DE VIDA.</b></p>
               <p>Visite nosso estande e conheça o apartamento decorado.</p>
         </div>
      </div>      
   </div>      

   <div class="container-fluid">
      <div class="row">
         <div class="col-lg-12">     
            <img src="img/empreendimentos/grand-villagio/pdf/02.png" alt="">
            <img src="img/empreendimentos/grand-villagio/pdf/03.png" alt="">
            <img src="img/empreendimentos/grand-villagio/pdf/04.png" alt="">
            <img src="img/empreendimentos/grand-villagio/pdf/05.png" alt="">
            <img src="img/empreendimentos/grand-villagio/pdf/06.png" alt="">
            <img src="img/empreendimentos/grand-villagio/pdf/07.png" alt="">
            <img src="img/empreendimentos/grand-villagio/pdf/08.png" alt="">
            <img src="img/empreendimentos/grand-villagio/pdf/09.png" alt="">
            <img src="img/empreendimentos/grand-villagio/pdf/10.png" alt="">
            <img src="img/empreendimentos/grand-villagio/pdf/11.png" alt="">
            <img src="img/empreendimentos/grand-villagio/pdf/12.png" alt="">
         </div>
      </div>
   </div>

   <div class="container mt-60">
      <div class="row gallery-row">      
         <div class="gallery-column">
            <img src="img/empreendimentos/grand-villagio/01.png">
            <img src="img/empreendimentos/grand-villagio/02.png">          
         </div>
         <div class="gallery-column">  
            <img src="img/empreendimentos/grand-villagio/03.png">                                      
         </div>     
         <div class="gallery-column">
            <img src="img/empreendimentos/grand-villagio/04.png"> 
         </div>     
         <div class="gallery-column">
            <img src="img/empreendimentos/grand-villagio/05.png">  
         </div>   
      </div>   
   </div>
      
</main>

<?php include ('layouts/white-footer.php'); ?>