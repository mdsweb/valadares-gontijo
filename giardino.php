<?php include ('layouts/head.php'); ?>

<?php include ('layouts/white-navbar.php'); ?>

<main class="white-page starter-page">

   <div class="container title-page">
      <div class="row">
         <div class="col col-3"><p class="line bg-blue"></p></div>
         <div class="col col-8">
            <h1 class="mt-20">Giardino</h1>
         </div>
      </div>      
   </div>      

   <div class="container">
      <div class="row gallery-row">      
         <div class="gallery-column">
            <img src="img/giardino/01-min.jpg">
            <img src="img/giardino/02-min.jpg">
         </div>
         <div class="gallery-column">       
            <img src="img/giardino/05-min.jpg">                               
         </div>     
         <div class="gallery-column">
            <img src="img/giardino/03-min.jpg">            
         </div>     
         <div class="gallery-column">
            <img src="img/giardino/04-min.jpg">
         </div>   
      </div>   
   </div>
      
</main>

<?php include ('layouts/white-footer.php'); ?>